\section{Results}
We empirically evaluate our methods against state-of-art methods on three tasks: link prediction\cite{NIPS2013_5071}, 
triplets classification\cite{NIPS2013_5028} and multimodal query retrieval\cite{JMLR:v15:srivastava14b}.
The first two tasks concentrate on whether the learned representations can embody the structural knowledge by evaluating result of link prediction and accuracy of triplets classification.
The third task multimodal retrieval is to evaluate if visual and textual knowledge have both been encoded in the learned representation.
Also, we preform link prediction and triplets classification tasks on out-of-knowledge-base(OOKB) entities to show that \modelName can learn valid representations for OOKB entities in zero-shot.
Furthermore, we analysis the role of multimodal knowledge during training, showing it brings improvement in both efficiency and accuracy.

\subsection{Datasets}

We harvest the existing knowledge graph WN9-IMG\cite{ijcai2017-438} with textual information to create a new knowledge base WN9-IMG-TXT consists of three parts:
entity-relation triplets, images of entities, and textual descriptions for entities,
among which the triplets and images are the same with WN9-IMG dataset.
To be more specific, triplets are a subset of WN18\cite{bordes2014semantic}, which is built on knowledge graph WordNet\cite{miller1995wordnet}. 
For visual knowledge, images are all extracted from ImageNet\cite{deng2009imagenet}, a large image database built according to WordNet hierarchy,
containing images for entities in WordNet. All entities in our dataset have images mapped to them with amount up to $10$.
Textual description is also extracted from WordNet, where definition is provided for each entity. 
This new dataset contains $9$ relation types and $6555$ entities.
Specific statistics is shown in Table \ref{statistics}.

\input{tableStatistics2}

Besides the statistics shown in Table \ref{statistics}, there are also $6555$ sentences and $63225$ images in it. We train and evaluate our model on WN9-IMG-TXT, and simply ignore the visual and textual descriptions when evaluating baseline methods.



\subsection{Experimental Settings}

\textbf{Baselines}\\
Since the triplet and image parts in our dataset are the same with WN9-IMG,
which is also used in \cite{ijcai2017-438}, we directly copy the experimental results of model TransE\cite{NIPS2013_5071},
TransR\cite{AAAI159571} and IKRL\cite{ijcai2017-438} as our baseline reuslts from this paper.

For models not evaluated in \cite{ijcai2017-438}, such as TransH\cite{AAAI148531}, TransD\cite{ji2015knowledge},
RESCAL\cite{nickel2011three} and HolE\cite{nickel2016holographic},
we train them on triplets of our dataset using the source code of OpenKE\footnote{https://github.com/thunlp/OpenKE},
following the optimal experimental settings claimed in their original papers.

\noindent \textbf{Implementation}\\
During training, we select learning rate $\learningRate$ among
$\left\{0.0001, 0.001, 0.01\right\}$, margin $\margin$ among 
$\left\{0.1, 0.5, 1.0, 2.0\right\}$, select optimizer among SGD,
RMSProp and Adam. Choose dissimilarity measure $\dissimilarity$ among
L1 and L2 measures. Choose the way replacing entities 
while creating corrupted triplets between ``uniform'' 
and ``bernoulli''. ``Bernoulli'' is a way to reduce false negative 
labels proposed in \cite{AAAI148531}, we strictly follow the 
instructions in this paper. For dimensions, 
dimension for visual and textual modality are both fixed 
in input layer, $\imageInput$ and $\textInput$ are set 
to be 4096 and 100 respectively. 
We select the first hidden layer dimension for visual
modality $\imageLatent$ among $\left\{512, 1024, 2048\right\}$ 
and $\left\{50, 100\right\}$ for textual modality $\textLatent$. 
Select hidden dimension $\dimension$ among 
$\left\{20, 50, 100\right\}$, which is also the dimension 
of relation representations. 
Note decoder and encoder have exactly same structure and 
dimensions for corresponding layers in autoencoder. 

The optimal configurations are: $\learningRate=0.001$, $\margin=0.5$.
We choose optimizer to be RMSProp, use $L1$ as dissimilarity measure,
and construct corrupted entities by the way "bernoulli".
for dimensions, $\imageLatent=512$, $\textLatent=50$, $\dimension=50$.
We limit the training times in $1000$ rounds.


\subsection{Link Prediction}

\textbf{Evaluation protocol}\\
Link prediction is a task first proposed in \cite{bordes2011learning} to predict
the missing entities in triplets,
and has been widely used to evaluate the learned representations.
We follow the same protocol as described in \cite{bordes2011learning}.
We first construct corrupted triplets by removing head entity
in each triplet and replace it with every other entities 
in dataset. Then rank these triplets in ascending order 
according to their dissimilarity scores.
We can get the rank for tail entities by removing tail entities instead of head following the same procedure.

Two metrics are used to evaluate: mean rank,
the average of predicted ranks for correct entities, 
and hits@10, proportion of original golden entities ranked in top 10.
These metrics are also referred to as ``raw'' mean rank and ``raw'' hits@10.
Corrupted triplets constructed this way may turn out to be correct,
as the triplets are 1-to-N and N-to-1, not 1-to-1 in dataset.
To eliminate this error, we remove all the candidates 
existed in the database, this leads to ``filtered'' mean rank
and ``filtered'' hits@10. 
For both raw and filtered settings, 
a smaller mean rank and a higher hits@10 indicates 
a representation with more capability.

\noindent \textbf{Experimental results}\\
The experimental results are shown in Table \ref{linkPrediction}. 
Our model outperforms all the baseline models that only use structural knowledge significantly, and receives competitive results with IKRL.

From the table we can observe that visual and textual knowledge bring large improvement in the results, 
both IKRL and our model achieve results far beyond others no matter they are based on translation or not, 
which proves the effectiveness of external sources of knowledge.
Also, our model is quite simple and flexible, can be combined with other more complicated models than TransE, which may leads to better results.

\input{tableLinkPrediction}

 
\subsection{Triplets Classification}

\textbf{Evaluation protocol}\\
Triplet classification aims to classify whether a triplet $(\head, \relation, \tail)$ is correct or not, 
based on the assumption that a triplet is correct if and only if it exists in the known knowledge graph. 
Following the same procedure in NTN\cite{NIPS2013_5028}, we construct a corresponding corrupted triplet for each golden triplet in knowledge base.
We also calculate a relation-specific threshold $\threshold$ for classification based on dissimilarity scores of all triplets.
We say a triplet is positive if its dissimilarity score is below the threshold $\threshold$ for the given relation $\relation$, and consider it negative otherwise.


\noindent \textbf{Experimental results}\\
We listed the results in Table \ref{classification}. 
It shows that our method gets better result than all the pervious approaches, also, the accuracy is high enough to discriminate golden triplets from corrupted ones in most cases.

\input{tableTripletClassification}



\subsection{Multimodal Query Retrieval}

While the above two tasks concentrate on evaluating if representations can capture structure information between entities and relations,
the query retrieval task is a classical task evaluating joint multimodal representations to check whether it encoded the multimodal knowledge.
It aims to find the top $n$ nearest entities in the low-dimensional representation space for a certain entity,
given its image and textual descriptions.
We follow the procedure described in \cite{JMLR:v15:srivastava14b}. 
In our experiment, we randomly select $6555$ images from our dataset as queries, 
with each belongs to a unique entity, combine them with corresponding text, use the image-text pairs as queries.

\input{figRetrieval}

We first pre-process images and text respectively for each image-text pair to get visual and textual representations, 
input these vectors to our model to get the entity representations. We use kNN\cite{altman1992introduction} model to get the nearest $4$ entities for certain entity.
To make comparison, we also use kNN to get the nearest $4$ entities in representation space get by TransE,
with the best parameter setting claimed in its original paper.
Part of our results are shown in Fig \ref{retrieval}. 
We found that the nearest entities TransAE found are also near in semantic, similar in visual representations,
indicates that when mapping entities and relations into embedding space, TransAE not only follow relational constraints,
but also group similar entities together. 
As TransE model values little on entities themselves, very different entities can be near to each other in embedding space, 
for example, for embeddings get TransE, the nearest neighbor of entity ``dressage'' are either fruits or armors,
which is not a desired illustration of our reality world.

\subsection{Zero-shot Learning for OOKB Entities}

Most existing translation based models can only perform link prediction task and triplet classification task on entities within knowledge graph, 
as for learning representations for out-of-knowledge-base(OOKB) entities has always been a challenging problem.
\begin{comment}
The only method that claim can solve this problem is DKRL\cite{AAAI1612216},
which can learn representations for OOKB entities based on textual descriptions.
IKRL\cite{ijcai2017-438}, though not claimed in the paper, can also achieve this goal as it can learn representations according to images of the entities.
\end{comment}
Making use of both textual and visual knowledge, our method can easily learn representations for all entities, no matter they are in knowledge graph or not.

We perform both link prediction task and triplet classification task on OOKB entities.
We split the WN9-IMG-TXT dataset into 4 datasets: new training dataset as the new knowledge base,
which is a subset of the original WN9-IMG-TXT dataset;
$1st$ test dataset where only head entity is out of knowledge base,
$2nd$ test dataset where only tail entity is out of knowledge base,
and the $3rd$ test dataset where head and tail entities are both out of knowledge base.
There are $6000$ triplets in the new training dataset, while $1000$ triplets per test dataset.
We perform both link prediction and triplet classification tasks on all these three datasets.


\noindent \textbf{Zero-shot Link Prediction}\\
In this task, we follow the exactly same protocol as in the normal link prediction task,
except that not all the entity representations are learned during training.
We learn representations for OOKB entities based on the multimodal representation model,
whose parameters are learned based on the entities in knowledge base.
\begin{comment}
For baseline methods DKRL and IKRL, we learn OOKB entity representations based on the textual information and visual knowledge respectively.
\end{comment}
We report the filtered mean rank among all $6555$ entities in Figure \ref{entityOOKB},
from which we can see that our method can get good results for entities not known before,
the results are even competitive to some traditional approaches on known entities according to Table \ref{linkPrediction}.

\input{tableLinkOOKB}

\noindent \textbf{Zero-shot Triplet Classification}\\
For this task, we use the same dataset as in zero-shot link prediction, 
following the same procedure as in the previous section.
Experiment results are shown in Figure \ref{classificationOOKB},
where the classification is only slightly lower than the case where all entities are known.

These experiments show TransAE can learn valid representations in zero-shot and achieve satisfactory results.

\input{tableClassificationOOKB}

\subsection{Further Analysis on Multimodal Knowledge}

To further analysis the influence of external sources of knowledge,
we adjust the relative weight relationship between autoencoder loss and TransE loss,
and train our model under different weight circumstance respectively.
Denote $w$ as the relative weight autoencoder loss compared to TransE loss,
we plot the training situation under $w=0.05, 0.15$ and $0.30$ in Figure \ref{analysis}.
It can be observed that as the weight of autoencoder increases,
model converges to a better result more quickly,
showing that external multimodal knowledge of entities can improve
both the representation quality and learning efficiency.

\input{figWeightAnalysis}

