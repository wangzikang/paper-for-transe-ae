\section{Introduction}
 
Knowledge has been playing an important role in artificial intelligence, 
as knowledge provide basis for machines to comprehend and accomplish human tasks.
Knowledge base, or knowledge graph, is usually used to store knowledge in a structured way.
Commonly used knowledge bases include WordNet\cite{miller1995wordnet}, Freebase\cite{bollacker2008freebase}, NELL\cite{carlson2010toward}.
These knowledge graphs provide solid data support for various knowledge-based applications 
such as question answering system, information retrieval, machine comprehension, etc.

Knowledge is represented as triplets in knowledge graph, 
denoted as $(\head, \relation, \tail)$, 
indicating head entity $\head$ and tail entity $\tail$ have relation $\relation$ between them.
Representation learning for knowledge is one of the most essential tasks in knowledge graph.
Various approaches have been explored to learn knowledge representations.
Translation approach\cite{NIPS2013_5071,AAAI148531,AAAI159571,ji2015knowledge} is one of the most popular methods,
it maps entities and relations into a low-dimensional continuous vector space,
where embeddings for entity $h$, $t$ and relation $r$ satisfy $h+r\approx t$. 
Translation methods have received huge success in knowledge representation. 

\input{figIntro}

However, most of the current approaches only considers the relational knowledge between entities,
which we also refer to ``structure'' knowledge in this paper\cite{AAAI1612216,ijcai2017-438}.
In this case, embeddings are only trained to satisfy relations with each other.
Information of entities themselves has merely been taken into account,
while these information like visual or textual descriptions are easy to access
and contains lots of valuable knowledge,
including both the knowledge contained explicitly in knowledge graph and
even the knowledge which are hard to be described as triplets.
Fig 1\subref{explicit} is an example of images representing knowledge explicitly 
stored in knowledge graph as $(\head, \relation, \tail)$, 
which is (chair, has part, leg) in this case;
In Fig 1\subref{implicit},
butterfly and flower all occurs at the same time,
indicating entity ``butterfly'' and ``flower'' are highly related
\footnote{All images in Figure 1 are from ImageNet\cite{deng2009imagenet}}. 
Information like this is obvious for human while hard to be captured and represented 
in knowledge graph. 
Explicitly encode visual and textual knowledge into entity representations 
can overcome this drawback to some extent, 
and bring significant improvement to the quality of learned representations 
according to our experiments.


To learn structural knowledge and multimodal knowledge jointly,
we propose a new knowledge representation learning model, \modelName,
by combing multimodal autoencoder(AE) and TransE. 
Based on this model, we can learn joint representations for entities given triplets
and their corresponding descriptions of images and text.
During experiments, we harvest the knowledge base WN9-IMG\cite{ijcai2017-438} with textual information,
to construct a knowledge graph contains not only triplets,
but also visual and textual knowledge for each entity.
We use feature vectors of text and images to represent multimodal knowledge
and feed them into our model to learn the desired representation. 


We evaluate our model on both knowledge graph representation tasks and 
multimodal representation tasks, including link prediction, 
triplet classification and multimodal query retrieval. 
On task link prediction and triplet classification, 
our model outperforms all methods concentrated only on structural knowledge 
with significant improvement, which demonstrates the usefulness of 
multimodal knowledge and the effectiveness of our method on 
learning relational knowledge between entities. 
By performing multimodal query retrieval task, 
we show that our learned representations have visual and textual knowledge encoded,
can aggregate similar entities together in the embedding space.
Furthermore, we also perform these tasks on out-of-knowledge-base entities,
where our approach can learn valid representations in zero-shot.
Also, by assigning different weights to autoencoder part in the loss function,
we show that extra knowledge source like text and images can improve the representation performance.


Our contributions are three-fold:
\begin{itemize}
\item We present \modelName, a knowledge representation learning model, 
	which can learn representations based on both structural knowledge and multimodal knowledge of triplets.
	We showed that the introduction of multimodal knowledge leads to huge improvement on model performance compared to traditional approaches.
\item We learn a unified representation for each entity based on all kinds of knowledge. 
	To the best of our knowledge, we are the first to obtain a joint representation from all these knowledge, 
	as the existing methods which make use of external knowledge learn structural embedding and other kinds of embeddings separately.
\item Despite receiving promising results on both knowledge graph representation tasks and multimodal representation tasks, 
	our model can also learn valid representations for entities out of knowledge base in zero shot, 
	which cannot be obtained in traditional translation-based methods.
\end{itemize}
